class Crop {
  constructor(c, x, y, w, h) {
    this.c = c;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;

    this.x1 = this.x + this.w;
    this.y1 = this.y + this.h;

    this.sw = w;
    this.sh = h;

    this.drag = false;
    this.dgx = -1;
    this.dgy = -1;

    this.ss = 20;
    this.drags = 0;
  }
  draw() {
    this.c.lineWidth = 3;
    this.c.clearRect(0, 0, this.sw, this.sh);
    this.c.beginPath();
    this.c.setLineDash([0]);
    this.c.strokeStyle = "#AAAAAA";
    this.c.strokeRect(this.x, this.y, this.x1 - this.x,  this.y1 - this.y);
    //this.c.rect(this.x, this.y, this.x1 - this.x,  this.y1 - this.y);
    this.c.setLineDash([6]);
    this.c.strokeStyle = "#000000";
    this.c.strokeRect(this.x, this.y, this.x1 - this.x,  this.y1 - this.y);
    //this.c.stroke();

    this.drawCP();
  }
  drawCP() {
    let w = this.x1 - this.x;
    let h = this.y1 - this.y;

    this.c.beginPath();
    this.c.setLineDash([0]);
    this.c.fillStyle = 'gray';
    this.c.fillRect(this.x, this.y, this.ss, this.ss);
    this.c.rect(this.x, this.y, this.ss, this.ss);
    this.c.stroke();
    this.c.beginPath();
    this.c.fillRect(this.x + w - this.ss, this.y, this.ss, this.ss);
    this.c.rect(this.x + w - this.ss, this.y, this.ss, this.ss);
    this.c.stroke();
    this.c.beginPath();
    this.c.fillRect(this.x, this.y + h - this.ss, this.ss, this.ss);
    this.c.rect(this.x, this.y + h - this.ss, this.ss, this.ss);
    this.c.stroke();
    this.c.beginPath();
    this.c.fillRect(this.x + w - this.ss, this.y + h - this.ss, this.ss, this.ss);
    this.c.rect(this.x + w - this.ss, this.y + h - this.ss, this.ss, this.ss);
    this.c.stroke();
  }
  mousedown(x, y) {
    var ss = 0;

    console.log('mousedown start.');

    if (this.incircle(x, y, this.x, this.y, this.ss)) {
      ss = 1;
    } else if(this.incircle(x, y, this.x1, this.y, this.ss)) {
      ss = 2;
    } else if(this.incircle(x, y, this.x, this.y1, this.ss)) {
      ss = 3;
    } else if(this.incircle(x, y, this.x1, this.y1, this.ss)) {
      ss = 4;
    } else {
      return;
    }

    this.drag = true;
    this.dgx = x;
    this.dgy = y;
    this.drags = ss;

    console.log('Drag in ' + this.drags);
  }
  mouseup(x, y) {
    this.drag = false;
    this.dgx = 0;
    this.dgy = 0;
    this.drags = 0;
    console.log('Drag free');
  }
  mousemove(x, y) {
    if (!this.drag)
      return;

    let w = this.x1 - this.x;
    let h = this.y1 - this.y;

    let rc = {x1: this.x, y1: this.y, 
              x2: this.x + w, y2: this.y + h};
    var dx = x - this.dgx;
    var dy = y - this.dgy;

    this.dgx = x;
    this.dgy = y;

    let ds_x = 2 * Math.abs(this.ss + 5);
    let ds_y = 2 * Math.abs(this.ss + 5);

    switch(this.drags) {
      case 1:
        //this.x += dx;
        //this.y += dy;
        //if (this.x < 0) this.x = 0;
        //if (this.y < 0) this.y = 0;
        //if (this.x > rc.x2) this.x = rc.x2 - this.ss;  
        //if (this.y > rc.y2) this.y = rc.y2 - this.ss;  
        if (((this.x + dx) < (this.x1 - ds_x)) && ((this.x + dx) > 0))
          this.x += dx;
        if (((this.y + dy) > 0) && ((this.y + dy) < (this.y1 - ds_y)))
          this.y  += dy;
        break;
      case 2:
        if (((this.x1 + dx) > (this.x + ds_x)) && ((this.x1 + dx) < this.w))
          this.x1 += dx;
        if (((this.y + dy) > 0) && ((this.y + dy) < (this.y1 - ds_y)))
          this.y  += dy;
        break;
      case 3:
        if (((this.x + dx) < (this.x1 - ds_x)) && ((this.x + dx) > 0))
          this.x += dx;
        if (((this.y1 + dy) < this.h) && ((this.y1 + dy) > (this.y + ds_y)))
          this.y1  += dy;
        break;
      case 4:
        if (((this.x1 + dx) > (this.x + ds_x)) && ((this.x1 + dx) < this.w))
          this.x1 += dx;
        if (((this.y1 + dy) < this.h) && ((this.y1 + dy) > (this.y + ds_y)))
          this.y1  += dy;
        break;
      }

    this.draw();
  }
  incircle(x, y, rx, ry, r) {
    if ((Math.abs(x - rx) < r ) && (Math.abs(y - ry) < r ))
      return true;
    return false;
  }
  cropRect(){
    return {x: this.x, y: this.y, w: this.x1 - this.x, h: this.y1 - this.y};
  }
}