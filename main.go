package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/ggvaberi/kjinmod"
)

// FnRoute is ...
type FnRoute func(http.ResponseWriter, *http.Request)

// WebRoute is ...
type WebRoute struct {
	id     string
	handle FnRoute
}

// WebHandler is ...
type WebHandler struct {
	mu sync.Mutex // guards n
	n  int

	routes []*WebRoute
}

// Add is ...
func (h *WebHandler) Add(id string, handle FnRoute) {
	var p = new(WebRoute)

	p.id = id
	p.handle = handle

	h.routes = append(h.routes, p)
}

func getValue(r *http.Request, key string) string {
	keys, ok := r.URL.Query()[key]

	if !ok || len(keys[0]) < 1 {
		log.Println("Url query key " + key + " is missing.")

		return ""
	}

	return keys[0]
}

func (h *WebHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var url = r.URL.Path

	log.Println("Serve url " + url)

	if len(url) > len("/static/") && url[0:8] == "/static/" {
		handleStatic(w, r)
	} else {
		log.Println("routes count " + strconv.Itoa(len(h.routes)))

		for _, rt := range h.routes {
			if rt.id == url {
				rt.handle(w, r)
			}
		}
	}
}

func handleIndex(w http.ResponseWriter, r *http.Request) {
	t := kjinmod.New()
	t.Init("index.html", "static/html")

	io.WriteString(w, t.Content)
}

func handleGetImage(w http.ResponseWriter, r *http.Request) {
	url := getValue(r, "url")

	log.Println("Handler get image for: " + url)

	res, err := http.Get(url)

	if err != nil {
		log.Fatalf("Get image error: http.Get -> %v", err)
	}

	// We read all the bytes of the image
	// Types: data []byte
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatalf("Get image error: ioutil.ReadAll -> %v", err)
	}
	//log.Println("Get image length: ", res.ContentLength)
	//data := make([]byte, res.ContentLength)
	//res.Body.Read(data)
	// You have to manually close the body, check docs
	// This is required if you want to use things like
	// Keep-Alive and other HTTP sorcery.
	res.Body.Close()

	w.Header().Set("Content-Length", fmt.Sprint(res.ContentLength)) /* value: 7007 */
	w.Header().Set("Content-Type", res.Header.Get("Content-Type"))  /* value: image/png */
	w.Write(data)
}

func handleCropImage(w http.ResponseWriter, r *http.Request) {
	url := getValue(r, "url")

	log.Println("Handler get image will search for: " + url)

	t := kjinmod.New()

	t.Init("crop.html", "static/html")

	t.Content = strings.ReplaceAll(t.Content, "{{ url }}", url)

	io.WriteString(w, t.Content)
}

func handleStatic(w http.ResponseWriter, r *http.Request) {
	data := readFile(r.URL.Path[1:])

	io.WriteString(w, data)
}

func readFile(path string) string {
	content, err := ioutil.ReadFile(path)

	if err != nil {
		log.Println("Cannot read file " + path)

		return ""
	}

	return string(content)
}

func main() {
	port := os.Getenv("PORT")

	if port == "" {
		port = "5000"
	}

	log.Println("Using port: " + port)

	var h = new(WebHandler)

	h.Add("/", handleIndex)
	h.Add("/getimage", handleGetImage)
	h.Add("/cropimage", handleCropImage)

	//h.Add("/sitemap.xml", handleSitemap)
	//h.Add("/BingSiteAuth.xml", handleBingSiteAuth)
	//h.Add("/googleb295dd6d4113b434.html", handleGoogleSiteAuth)

	http.Handle("/", h)
	http.ListenAndServe(":"+port, nil)
}
